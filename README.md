# Arduino Snake game

## Podlaczenie element�w uk�adu

### Rozszczepienie zasilania 5V dla wszystkich modulow

- Podlacz zolty kabel do Arduino 5V, drugi koniec do plytki stykowej, gdziekolwiek w linii "+"

### Kontroler LED 8x8

- (Zasilanie) Podlacz VCC do jakiegokolwiek pola w linii "+" na plytce stykowej

- (Uziemienie) Podlacz GND do jakiegokolwiek pola GND na Arduino

- (Wejscie cyfrowe) Podlacz DIN do zlacza 12 na Arduino

- (Zegar) Podlacz CLK do zlacza 11 na Arduino

- (Chip Select) Podlacz CS do zlacza 10 na Arduino


### Joystick

- (Zasilania) Podlacz +5V do jakiegokolwiek pola w linii "+" na plytce stykowej

- (Uziemienie) Podlacz GND do jakiegokolwiek pola GND na Arduino

- (X) Podlacz VRx do A0 na Arduino

- (Y) Podlacz VRy do A1 na Arduino

- Pozostaw SW jako wolne zlacze

## Instalowanie Arduino IDE
Pobierz i zainstaluj Arduino IDE z nastepujacej strony:
https://www.arduino.cc/en/Main/Software

## Instalowanie biblioteki LedControl - wymagana do obslugi LED 8x8
Instalacja biblioteki najlatwiej przebiega korzystajac z Arduino IDE. Kroki znajdziesz na stronie: https://www.arduino.cc/en/Guide/Libraries. Nazwa biblioteki "LedControl".
Pobierz zip ze strony: https://www.arduinolibraries.info/libraries/led-control