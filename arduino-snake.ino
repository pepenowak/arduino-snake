#include "LedControl.h"

LedControl lc = LedControl(12, 11, 10, 1); //(DIN, CLK, CS, Amount of MAX7219)

int joystickAnalogPinX = 0; //A0 on the board
int joystickAnalogPinY = 1; //A1 on the board
int xAxis = 0; // value in range 0-8 that represents joystick x position (4 is center)
int yAxis = 0; // value in range 0-8 that represents joystick y position (4 is center)

String snakeDirection; // "up" or "down" or "left" or "right";

// snake maximum size is 36 dots
int snakeX[36]; // x coordinates of all snake parts
int snakeY[36]; // y coordinates of all snake parts
int snakeSize; // total size

int speed = 300; // delay between moves (in ms)

int foodX; // x coordinate of food
int foodY; // y coordinate of food

boolean isGameInProgress = false;

void setup() {
  lc.shutdown(0, false);
  lc.setIntensity(0, 8);
  lc.clearDisplay(0);
  Serial.begin(9600);
  newGame();
}

void loop() {
  if (isGameInProgress) {
    lc.clearDisplay(0); // clear screen in order to print current state

    xAxis = simple(analogRead(joystickAnalogPinX));
    yAxis = simple(analogRead(joystickAnalogPinY));

    if (yAxis > 5 && snakeDirection != "up") {
      snakeDirection = "down";
    }
    if (yAxis < 3 && snakeDirection != "down") {
      snakeDirection = "up";
    }
    if (xAxis > 5 && snakeDirection != "left") {
      snakeDirection = "right";
    }
    if (xAxis < 3 && snakeDirection != "right") {
      snakeDirection = "left";
    }

    move(snakeDirection);

    checkIfHitFood();
    checkIfHitSelf();

    drawSnake();
    drawFood();

    delay(speed);
  }
}

//This method returns a more simple value to work with when reading the analogs.
//Returns and int that is 0-8.
int simple(int num) {
  return (num * 9 / 1024);
}

void move(String dir) {
  for (int i = snakeSize - 1; i > 0; i--) { // foreach dot in snake body, set its position to previous body part position
    snakeX[i] = snakeX[i - 1];
    snakeY[i] = snakeY[i - 1];
  }

  if (dir == "up") {
    if (snakeY[0] == 0) {
      snakeY[0] = 7;
    } else {
      snakeY[0]--;
    }
  } else if (dir == "down") {
    if (snakeY[0] == 7) {
      snakeY[0] = 0;
    } else {
      snakeY[0]++;
    }
  } else if (dir == "left") {
    if (snakeX[0] == 0) {
      snakeX[0] = 7;
    } else {
      snakeX[0]--;
    }
  } else if (dir == "right") {
    if (snakeX[0] == 7) {
      snakeX[0] = 0;
    } else {
      snakeX[0]++;
    }
  }
}

void drawSnake() {
  for (int i = 0; i < snakeSize; i++) {
    lc.setLed(0, snakeY[i], snakeX[i], true);
  }
}

void drawFood() {
  lc.setLed(0, foodY, foodX, true);
  delay(50);
  lc.setLed(0, foodY, foodX, false);
}

void newFood() {
  int newFoodX = random(0, 8);
  int newFoodY = random(0, 8);
  while (isSnake(newFoodX, newFoodY)) {
    newFoodX = random(0, 8);
    newFoodY = random(0, 8);
  }
  foodX = newFoodX;
  foodY = newFoodY;
}

void checkIfHitFood() {
  if (snakeX[0] == foodX && snakeY[0] == foodY) {
    snakeSize++;
    newFood();
  }
}

void checkIfHitSelf() {
  for (int i = 1; i < snakeSize - 1; i++) {
    if (snakeX[0] == snakeX[i] && snakeY[0] == snakeY[i]) {
      gameOver(); //Call the gameOver() method.
    }
  }
}

boolean isSnake(int x, int y) {
  for (int i = 0; i < snakeSize; i++) {
    if ((x == snakeX[i]) && (y == snakeY[i])) {
      return true;
    }
  }
  return false;
}

void newGame() {
  for (int i = 0; i < 36; i++) {
    snakeX[i] = -1;
    snakeY[i] = -1;
  }

  snakeX[0] = 4;
  snakeY[0] = 8;
  snakeDirection = "up";
  snakeSize = 1;
  newFood();
  isGameInProgress = true;
}

void gameOver() {
  isGameInProgress = false;
  for (int row = 0; row < 8; row++) {
    for (int col = 0; col < 8; col++) {
      lc.setLed(0, col, row, true);
      delay(25);
    }
  }

  for (int row = 0; row < 8; row++) {
    for (int col = 0; col < 8; col++) {
      lc.setLed(0, col, row, false);
      delay(25);
    }
  }
  newGame();
}

